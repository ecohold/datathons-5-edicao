Projeto Detalhado de Módulo de Eletrohidrólise e Célula de Combustível PEM - H2Edu

Cliente: SENAI, Rede Pública de Ensino

Estratégia: Tripé TRI: Tecnologia; Regulamentação; Investimento

Regulamentação: apresentação dos kits nas câmaras municipais, estaduais e federais estabelece bases para suprimentos dos kits na rede de ensino público/privado; cultura do produto, formação e industrialização

Competição: Carrinhos a H2 municipal, estadual, nacional e internacional -> Stock Cars à H2
https://www.instagram.com/reel/C47vRQ7ImJo/?utm_source=ig_web_copy_link

Objetivo: Desenvolver um projeto padrão de projeto e construção de um módulo de eletrohidrólise e uma célula de combustível PEM para fins de ensino didático.

Escopo:

Módulo de Eletrohidrólise:
Talhamento de eletrodos
Montagem da pilha eletrolítica
Sistema de alimentação de água
Sistema de coleta de hidrogênio
Célula de Combustível PEM:
Talhamento da membrana de troca de prótons (PEM)
Montagem da pilha de combustível
Sistema de alimentação de hidrogênio
Sistema de coleta de água
Fases do Projeto:

Fase 1: Projeto Conceitual

Definição dos requisitos do projeto
Seleção de materiais e componentes
Criação de diagramas conceituais
Diagrama:

Diagrama Conceitual do Módulo de Eletro Hidrólise
Diagrama Conceitual da Célula de Combustível PEM

Fase 2: Projeto de Detalhes

Desenvolvimento de desenhos detalhados
Especificação de componentes
Criação de modelos 3D
Diagrama:

Desenho Detalhado do Módulo de Eletrohidrólise
Desenho Detalhado da Célula de Combustível PEM

Fase 3: Fabricação e Montagem

Fabricação de componentes
Montagem do módulo de eletrohidrólise e da célula de combustível PEM
Fase 4: Teste e Avaliação

Teste do desempenho do módulo de eletrohidrólise e da célula de combustível PEM
Avaliação da eficiência e durabilidade
Fase 5: Documentação

Criação de manuais de usuário e guias de manutenção
Documentação do projeto para fins de ensino
CAPEX, OPEX e ROI

CAPEX (Despesas de Capital):

Custo dos materiais e componentes
Custo da fabricação
Custo da montagem
OPEX (Despesas Operacionais):

Custo da energia elétrica
Custo da água
Custo da manutenção
ROI (Retorno sobre o Investimento):

Valor educacional do projeto
Potencial para pesquisa e desenvolvimento
Benefícios ambientais da produção de hidrogênio verde

Unidades do SENAI que podem participar na elaboração de cada fase do projeto detalhamento de módulo de eletrohidrólise e célula de combustível PEM:

Fase 1: Projeto Conceitual

Unidade de Eletrotécnica
Unidade de Química
Unidade de Mecatrônica
Fase 2: Projeto de Detalhes

Unidade de Desenho Industrial
Unidade de Engenharia Mecânica
Unidade de Engenharia Elétrica
Fase 3: Fabricação e Montagem

Unidade de Usinagem
Unidade de Soldagem
Unidade de Montagem
Fase 4: Teste e Avaliação

Unidade de Eletrotécnica
Unidade de Química
Unidade de Mecatrônica
Fase 5: Documentação

Unidade de Documentação Técnica
Unidade de Educação Profissional
Todas as unidades do SENAI podem participar da elaboração do projeto, pois ele envolve conhecimentos multidisciplinares. No entanto, as unidades listadas acima possuem expertise específica nas áreas necessárias para cada fase do projeto.


CC: Lelê, vc com o produto, vc agenda apresentação na cmsp, alesp e esfera federal, especialmente em marica, rj onde o pib per capita foi de US$2000 em 2010 para US$45.000 em 2019 graças ao pré sal, agora, h2 está na rota estratégica da cidade.


Cor	Insumo Principal	Processo	Vantagens	Desvantagens	


Verde (H2V)	Água	Eletrohidrólise	Emissão zero de carbono, processo limpo e renovável	
Alto custo de produção, infraestrutura em desenvolvimento



Azul (H2A)	Gás natural	Reforma a vapor com captura e armazenamento de carbono (CCS)	Tecnologia madura, infraestrutura existente	
Emissões de CO2, dependência de combustíveis fósseis



Cinza (H2G)	Gás natural	Reforma a vapor sem captura de carbono	Baixo custo de produção	
Emissões significativas de CO2, impacto ambiental negativo



Turquesa (H2BtL)	Biomassa	Gasificação e liquefação de biomassa	Emissão zero de carbono, uso de recursos renováveis	
Tecnologia em desenvolvimento, custos de produção elevados



Rosa (H2N)	Amônia	Craqueamento de amônia	Infraestrutura de amônia existente, transporte facilitado	
Emissões de CO2 se a amônia não for produzida de forma verde



Preto (H2F)	Carvão mineral	Gasificação de carvão com captura de carbono	Tecnologia madura, reservas abundantes de carvão	
Emissões de CO2, impacto ambiental negativo



