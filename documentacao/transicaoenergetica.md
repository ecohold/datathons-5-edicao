TRANSIÇÃO ENERGÉTICA

https://www2.aneel.gov.br/cedoc/adsp2024778_2.pdf

ANEEL confirma prazos da Chamada de Projeto de PDI Estratégico sobre hidrogênio no setor elétrico
Interessados em participar devem demonstrar interesse até quinta-feira (28).
Compartilhe:  Compartilhe por Facebook Compartilhe por Twitter Compartilhe por LinkedIn Compartilhe por WhatsApplink para Copiar para área de transferência
Publicado em 26/03/2024 08h59 Atualizado em 26/03/2024 10h52
AAgência Nacional de Energia Elétrica (ANEEL) publicou na última segunda-feira (18/3), o edital de Chamada de Projeto Estratégico de Pesquisa, Desenvolvimento e Inovação (PDI) com foco em hidrogênio e seus impactos no setor elétrico brasileiro. O objetivo é estimular o desenvolvimento de tecnologias nacionais na cadeia do hidrogênio, aproveitando o potencial elétrico, geográfico, científico, tecnológico e econômico do país.

O edital prevê duas modalidades de projetos: peças e componentes, com foco no desenvolvimento ou nacionalização de tecnologias que contribuam para a eficiência energética dos processos de conversão e armazenamento de hidrogênio; e planta piloto, que inclui a construção de uma unidade de produção de hidrogênio a partir de fontes renováveis como hidráulica, solar, eólica e biomassa, com potência entre 1 MW e 10 MW.

Entre os resultados esperados estão identificar impactos e oportunidades do hidrogênio para o setor elétrico, o mapeamento de obstáculos regulatórios, o desenvolvimento de soluções tecnológicas nacionais, a criação de redes de inovação e o desenvolvimento de modelos de negócio. Os projetos devem ser realizados em até 48 meses por empresas do setor elétrico em parceria com indústrias, startups, universidades e instituições de pesquisa.

O cronograma da chamada prevê os seguintes prazos a partir da publicação do aviso: 10 dias para manifestação de interesse das empresas, 90 dias para envio das propostas, 21 dias para apresentação em workshop, 51 dias para divulgação do resultado da avaliação inicial, 10 dias para confirmação de execução pelas proponentes e 120 dias para início da execução dos projetos aprovados. Como o despacho publicado pela diretoria já aprova a chamada, a sua data de publicação já é a data a partir da qual começa a contar o prazo de 10 dias para demonstração de interesse. Temos então o seguinte cronograma com respectivas datas das etapas da Chamada:

Fase	Datas
Aprovação da Chamada pela diretoria em RPO	12/3/2024
Publicação no DOU do edital da Chamada Estratégica	18/3/2024
Demonstração de interesse em financiar o projeto (pelas empresas do setor elétrico)	28/3/2024
Divulgação das empresas interessadas em financiar o projeto (pela ANEEL)	2/4/2024
Envio de proposta de projeto à ANEEL (pelas entidades proponentes)	1º/7/2024
Workshop para apresentação da proposta de projeto (pelas entidades proponentes)	22/7/2024
Divulgação do resultado da avaliação inicial da proposta de projeto (pela diretoria da ANEEL)	16/9/2024
Demonstração de interesse na execução do projeto (pelas entidades proponentes)	26/9/2024
Início da execução do projeto (pelas entidades proponentes)	24/1/2025
Essa iniciativa da ANEEL busca apoiar a transição energética e a descarbonização da economia, incentivando pesquisas para posicionar o Brasil como um importante player no mercado global de hidrogênio de baixa emissão. Espera-se que os projetos colaborem para compreender e superar os desafios da integração dessa fonte energética no sistema elétrico nacional.

Categoria
Energia Elétrica