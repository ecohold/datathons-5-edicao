Próximos Passos:

1) Coletar e organizar os dados.
2) Treinar o modelo de previsão de demanda.
3) Desenvolver a API para disponibilizar as previsões em tempo real.
3) Criar dashboards interativos para visualização dos dados.
4) Implementar o sistema de alertas.

`- appNetEnergy/
    - apis/
        - previsao_oferta_demanda.py
        - ...
    - dados/
        - carga/
            - carga_diaria.csv
        - geracao/
            - geracao_por_tipo.csv
            - geracao_mmgd_diaria.csv
        - MMGD/
            - localizacao_usinas_mmgd.csv
        - outros/
            - previsao_tempo.api
            - condicoes_economicas.api
    - modelos/
        - modelo_previsao.py
        - parametros.ini
    - APIs/
        - app.py
        - requirements.txt
    - documentacao/
        - README.md
        - Relatorio_Tecnico.pdf



{

DATATHONS - 5ª EDIÇÃO
O ONS – Operador Nacional do Sistema Elétrico – lança desafio sobre os impactos da Micro e Minigeração distribuída (MMGD) na operação em tempo real do SIN – Sistema Interligado Nacional.

Título do Desafio: Impactos das fontes de energia renovável na Operação em Tempo Real do Sistema Interligado Nacional (SIN).

As energias renováveis representam fontes fundamentais impulsionando a transição para sistemas mais sustentáveis. Neste contexto, pode-se destacar o conceito de Micro e Minigeração Distribuída (MMGD), que envolve a produção descentralizada de energia, permitindo que consumidores individuais ou comunidades gerem parte ou toda a energia de que precisam localmente.

Regulamento: Leia o regulamento para a contextualização do assunto tratado no desafio, o que é o desafio e o protótipo a ser elaborado como produto final.

Outras informações sobre a agenda do Desafio:

Welcome Day - 22/03 às 18h e 30
DatathONS - 23 e 24/03 das 9h às 18h
PitchDay - 26/03 às 18h
SOBRE O PROGRAMA A transição energética e a digitalização da tecnologia fazem parte de uma grande jornada de transformações no Setor Elétrico. Por isso, o ONS – Operador Nacional do Sistema Elétrico - quer conhecer profissionais que ofereçam soluções para resolver desafios reais.

A transição energética e a digitalização da tecnologia fazem parte de uma grande jornada de transformações no Setor Elétrico. Por isso, o ONS possui o InovathONS, um dos pilares do programa de Inovação através da Diretoria de TI, Relacionamento com Agentes e Assuntos Regulatórios (DTA), que tem como objetivo fomentar a modernização/inovação lançando desafios e buscando soluções inovadoras para resolvê-los, isto é, um programa de exploração de novas ideias com sucesso – o nosso DatathONS 5ª edição.

Dados e recursos
Regulamento DATATHONS - 5ªEdicaoPDF
Dados de Carga VerificadaZIP
Dias EspeciaisZIP
Dados de Carga Sem MMGDZIP
Capacidade Instalada de Usina MMGDZIP
DatathONS
Informações Adicionais
Fonte
https://datathons5aedicao.liga.ventures/
Última Atualização
22 de março de 2024, 17:59 (UTC-03:00)
Criado
18 de março de 2024, 17:11 (UTC-03:00)
Aviso Legal
Ao acessar este site e/ou utilizar as informações provenientes dele, será considerado que você aceitou os termos e condições da LICENÇA CC-BY, que permite que os reutilizadores distribuam, modifiquem, adaptem e desenvolvam o material sobre os dados, desde que seja dado o crédito apropriado ao criador(ONS) e que informe quais alterações foram feitas. Os dados são fornecidos "como estão" e apenas para fins informativos; e este conjunto de dados pode ter sua disponibilidade interrompida a qualquer momento e por qualquer motivo
Aviso Legal: Licença
http://opendefinition.org/od/2.1/pt-br
}