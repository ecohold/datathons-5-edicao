MERCADO

Aprovada Consulta Pública para discutir as alternativas de cálculo da energia requerida e das perdas não técnicas considerando os efeitos da MMGD
As contribuições devem ser enviadas no período de 28/3 a 13/5 para o e-mail: cp009_2024@aneel.gov.br
Compartilhe:  Compartilhe por Facebook Compartilhe por Twitter Compartilhe por LinkedIn Compartilhe por WhatsApplink para Copiar para área de transferência
Publicado em 26/03/2024 15h59 Atualizado em 26/03/2024 16h00
ADiretoria da Agência Nacional de Energia Elétrica (ANEEL) aprovou nesta terça-feira (26/3), durante Reunião Pública, abertura de Consulta Pública para discutir Análise de Impacto Regulatório (AIR) das alternativas de cálculo da energia requerida e das perdas não técnicas nos sistemas de distribuição de energia elétrica, considerando os efeitos da Minigeração e Microgeração Distribuída (MMGD) e a necessidade de padronização dos dados do Balanço Energético de perdas.

Ao longo dos últimos anos, em razão do crescimento do mercado de MMGD, ocorreu uma maior disparidade de montantes entre os mercados de baixa tensão faturado e medido, com impactos para a forma de apuração das perdas totais, que consideram tanto o mercado medido (perdas totais “medidas”) como o mercado faturado (perdas totais “faturadas”).

Em resumo, a energia da MMGD está interferindo nos montantes regulatórios da energia requerida e das perdas não técnicas dos processos tarifários, afetando o reconhecimento dessa receita das distribuidoras e as tarifas pagas pelos consumidores, o que se tornou um problema regulatório. A abertura da Consulta Pública foi precedida pela Tomada de Subsídios 28, que tratou do aprimoramento dos dados do balanço energético para apuração das perdas de energia, e averiguou com os agentes a possibilidade de homologação das perdas não técnicas sobre o mercado de baixa tensão medido. A TS 28 recebeu contribuições de 17 agentes.

A Consulta propõe a discussão dos seguintes pontos: Relatório de Análise de Impacto Regulatório (AIR) sobre a proposta de alternativas para o cálculo da energia requerida e das perdas não técnicas, considerando os efeitos da MMGD; proposta de atualização do regulamento disposto nos Procedimentos de Regulação Tarifária – Proret, conforme encaminhamentos da AIR; propostas de padronização e melhorias das informações fornecidas na modalidade Balanço de Energia (SAMP Balanço).

As contribuições devem ser enviadas no período de 28/3 a 13/5 para o e-mail: cp009_2024@aneel.gov.br. 

Categoria
Energia, Minerais e Combustíveis