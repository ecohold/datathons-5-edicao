# DATATHONS - 5ª EDIÇÃO

## Estrutura de Pastas e Arquivos para Previsão em Tempo Real de Oferta e Demanda de Energia

**Objetivo:**

Aprimorar o modelo atual para prever em tempo real a oferta e demanda de energia elétrica no Brasil, incorporando dados da Micro e Minigeração Distribuída (MMGD) do Operador Nacional do Sistema Elétrico (ONS).

**Funcionalidades:**

* **Aquisição de Dados:**
    * Obter dados da MMGD do ONS em tempo real através da API.
    * Coletar dados de carga, geração e outros fatores relevantes de fontes públicas.
    * Armazenar os dados em um formato estruturado e acessível.
* **Previsão de Demanda:**
    * Utilizar um modelo de Machine Learning para prever a demanda de energia em tempo real, considerando a sazonalidade, eventos climáticos, feriados e outros fatores relevantes.
    * Integrar dados da MMGD para estimar sua contribuição para a demanda total.
* **Previsão de Oferta:**
    * Prever a oferta de energia em tempo real, considerando a geração por tipo de fonte (hidrelétrica, eólica, solar, etc.), a capacidade instalada e as condições de operação das usinas.
    * Incluir a geração da MMGD na previsão da oferta, estimando sua produção com base na capacidade instalada, nas condições climáticas e na localização das usinas.
* **Visualização:**
    * Criar dashboards interativos para visualizar a demanda, oferta e outras variáveis relevantes em tempo real.
    * Mostrar a previsão da demanda e oferta de energia, com intervalos de confiança.
    * Identificar áreas com risco de déficit ou excedente de energia.
* **Alertas:**
    * Gerar alertas em tempo real para operadores do sistema elétrico em caso de risco de desequilíbrio entre oferta e demanda.
    * Notificar os usuários sobre possíveis impactos na qualidade do fornecimento de energia.

**Estrutura da Pasta de Arquivos:**

    `- appNetEnergy/
        - apis/
            - previsao_oferta_demanda.py
            - requirements.txt
            - api.py
        - dados/
            - carga/
                - carga_diaria.csv
            - geracao/
                - geracao_por_tipo.csv
                - geracao_mmgd_diaria.csv
            - MMGD/
                - localizacao_usinas_mmgd.csv
            - outros/
                - previsao_tempo.api
                - condicoes_economicas.api
        - modelos/
            - modelo_previsao.py
            - parametros.ini
    
       - documentacao/

               ├── Chamadas.md
               ├── dataset.md
               ├── kiteletrolise.md
               ├── READEME.md
               └── transicaoenergetica.md

        - ...
    ```

**Observações:**

* Esta é uma proposta inicial, e a estrutura da pasta de arquivos pode ser ajustada de acordo com as necessidades específicas do projeto.
* É importante considerar a segurança e a confiabilidade do sistema, especialmente ao lidar com dados em tempo real.
* A documentação deve ser clara e completa, para facilitar o uso e a manutenção do modelo.

**Recursos Adicionais:**

* **Biblioteca Pandas:** [https://pandas.pydata.org/](https://pandas.pydata.org/)
* **Biblioteca TensorFlow:** [https://www.tensorflow.org/](https://www.tensorflow.org/)
* **API do ONS:** [https://www.ons.org.br/](https://www.ons.org.br/)
* **Datathon do ONS:** [URL inválido removido]

**Próximos Passos:**

1. Coletar e organizar os dados.
2. Treinar o modelo de previsão de demanda.
3. Desenvolver a API para disponibilizar as previsões em tempo real.
4. Criar dashboards interativos para visualização dos dados.
5. Implementar o sistema de alertas.

Objetivo:

Esta estrutura de pastas e arquivos visa adaptar o projeto de Previsão em Tempo Real de Oferta e Demanda de Energia para o uso de serviços equivalentes gratuitos sob MAAS (Metal as a Service) no Ubuntu 22.

Serviços Equivalentes:

Serviço AWS	Serviço Ubuntu MAAS	Descrição
Amazon ECS	Kubernetes	Orquestração de contêineres
Amazon SageMaker	Kubeflow Pipelines	Treinamento e implantação de modelos de Machine Learning
AWS Lambda	Apache OpenWhisk	Funções sem servidor
Amazon Redshift	Apache Pinot	Data warehouse de alto desempenho

Observações:

A pasta manifests foi adicionada para armazenar os manifestos do Kubernetes para a implantação dos contêineres.
O arquivo requirements.txt foi atualizado para incluir as bibliotecas necessárias para o funcionamento do projeto no Ubuntu MAAS.
O arquivo README.md foi atualizado com as instruções de instalação e configuração para o Ubuntu MAAS.
Próximos Passos:

Coletar e organizar os dados.
Treinar o modelo de previsão de demanda.
Criar os manifestos do Kubernetes.
Implantar o aplicativo no cluster MAAS.
Configurar o sistema de alertas.
Equipe:

Nome dos Integrantes
Agradecimentos:


# Requisitos # 

## Resumo Detalhado do Desafio da Live ONS:

**Introdução:**

A Live ONS abordou os impactos da micro e minigeração distribuída (MMGD) na operação em tempo real do Sistema Interligado Nacional (SIN), os desafios de apagão e a solução ONSapi.

**Palestrantes:**

* Gabriel (Engenheiro de Previsão e Acompanhamento de Carga)
* Danilo (Engenheiro de Previsão e Acompanhamento de Carga)
* Janine (Analista de Dados e Soluções Analíticas)
* Taiane (Engenheira de Produção e Facilitadora)
* Marcos (Engenheiro Eletricista e Facilitador)

**Apresentação:**

**1. Impactos da MMGD na Curva de Carga:**

* Aumento da geração solar durante o dia, levando a um "afundamento" na curva de carga.
* Aumento do consumo à noite, com o uso de ar-condicionado e outros eletrodomésticos.
* Rampas de carga mais acentuadas, exigindo maior flexibilidade do sistema.
* Desafios na previsão de carga e no despacho de geração.

**2. Desafios de Apagão:**

* Eventos climáticos extremos, como frentes frias e secas.
* Falhas em usinas ou linhas de transmissão.
* Aumento da demanda por energia.
* A necessidade de garantir a segurança e confiabilidade do sistema.

**3. Solução ONSapi:**

* Plataforma de dados abertos do ONS, com informações sobre carga, geração, potência instalada, etc.
* Permite a criação de soluções inovadoras para os desafios do setor elétrico.
* O desafio final da live consistia em quantificar e metrificar as rampas de carga utilizando dados da ONSapi.

**4. Portal de Dados Abertos do ONS:**

* [URL inválido removido]
* Oferece 50 conjuntos de dados em formato CSV e Excel.
* Abrange as áreas de avaliação da operação, integração de instalações, planejamento da operação e programação.

**5. Desafio Final:**

* Quantificar e metrificar as rampas de carga com base em dados de minuto a minuto da ONSapi.
* Considerar diferentes tipos de dias, subsistemas e períodos do ano.
* Desenvolver soluções para mitigar os impactos da MMGD e aumentar a confiabilidade do sistema.

**6. Perguntas e Respostas:**

* A live contou com um chat interativo, onde os participantes puderam enviar perguntas aos palestrantes.

**7. Próximos Passos:**

* As soluções para o desafio final serão avaliadas por uma equipe do ONS.
* Os melhores trabalhos serão premiados.
* As informações da live e do desafio estão disponíveis no canal do ONS no YouTube.

**Conclusão:**

A live abordou os principais desafios do setor elétrico brasileiro e apresentou a ONSapi como uma ferramenta para o desenvolvimento de soluções inovadoras. O desafio final incentiva a participação da comunidade na busca por soluções para a operação do sistema elétrico.

**Palavras-chave:**

* MMGD
* Apagão
* ONSapi
* Desafio
* Solução
* Setor elétrico
* Brasil

**Observações:**

* A live foi gravada e está disponível no canal do ONS no YouTube.
* O material da live, incluindo as apresentações e o desafio final, também está disponível no site do ONS.

**Links Úteis:**

* Canal do ONS no YouTube: [URL inválido removido]
* Portal de Dados Abertos do ONS: [URL inválido removido]
* Site do ONS: [https://www.ons.org.br/](https://www.ons.org.br/)

**Desafios Anteriores:**

* **Versão 1:**
    * Estimativa das rampas de carga no SIN, considerando a geração da MMGD.
    * Avaliação do impacto da MMGD nas rampas de carga em diferentes cenários.
    * Identificação dos subsistemas e horários mais críticos em termos de rampas de carga.
    * Proposição de soluções para mitigar os impactos da MMGD nas rampas de carga.
* **Versão 2:**
    * Quantificar e metrificar as rampas de carga com base em dados de minuto a minuto da ONSapi.
    * Considerar diferentes tipos de dias, subsistemas e períodos do ano.
    * Desenvolver soluções para mitigar os impactos da MMGD e aumentar a confiabilidade do sistema.

**Distribuidoras de Energia no Brasil:**

O Brasil tem 68 distribuidoras de energia, divididas por estado. A lista completa pode ser consultada no site da ANEEL: https://www.


#------------------------------------------------#

# Datasets
Ao ONS por fornecer os dados da MMGD.
À equipe do DatathONS pelo desafio e pela oportunidade de contribuir para a pesquisa em energia.
Recursos Adicionais:

Documentação do Kubernetes: https://kubernetes.io/docs/home/
Documentação do Kubeflow Pipelines: https://www.kubeflow.org/docs/pipelines/
Documentação do Apache OpenWhisk: [URL inválido removido]
Documentação do Apache Pinot: [URL inválido removido]
Observações Adicionais:

Esta é uma adaptação inicial, e a estrutura da pasta de arquivos e os serviços equivalentes podem ser ajustados de acordo com as necessidades específicas do projeto.
É importante considerar a segurança e a confiabilidade do sistema, especialmente ao lidar com dados em tempo real.
A documentação deve ser clara e completa, para facilitar o uso e a manutenção do modelo.


